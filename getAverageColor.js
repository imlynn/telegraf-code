const getAverageColor = url => {
	const createContext = () => {
		const c = document.createElement('canvas');
		return c.getContext
				? c.getContext('2d')
				: false;
	}

	const _ = (url, ctx) => {
		return new Promise((resolve, reject) => {
			let img = new Image();
			img.src = url;
			img.onload = () => {
				if(!ctx)
					reject(false);

				const w = ctx.canvas.width = img.width;
				const h = ctx.canvas.height = img.height;
				const blockSize = 5;
				ctx.drawImage(img, 0, 0);
				let data = ctx.getImageData(0, 0, w, h),
					d = data.data,
					l = d.length,
					i = -4,
					rgba = {r:0, g:0, b:0},
					c = 0;
				while( (i += blockSize*4) < l ){
					++c;
					rgba.r += d[i];
					rgba.g += d[i+1];
					rgba.b += d[i+2];
				}

				//~~ is for rounding
				rgba.r = ~~(rgba.r/c);
				rgba.g = ~~(rgba.g/c);
				rgba.b = ~~(rgba.b/c);

				let resp = '#' + ('0' + parseInt(rgba.r,10).toString(16)).slice(-2) +
						   ('0' + parseInt(rgba.g,10).toString(16)).slice(-2) +
						   ('0' + parseInt(rgba.b,10).toString(16)).slice(-2);
				//returns hexa: #rrggbbaa
				resolve(resp);
			}
			img.onerror = (e) => {
				reject(e);
			}
		});
	}
	const ctx = createContext();
	return _(url, ctx);
}

/*example of use*
window.addEventListener('load', (function(){
	let url = document.getElementById('image').src;
	getAverageColor(url).then(response => {
		document.body.style.background = response;
	});
}));*/